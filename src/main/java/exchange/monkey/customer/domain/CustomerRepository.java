package exchange.monkey.customer.domain;

import org.springframework.data.jpa.repository.JpaRepository;

interface CustomerRepository extends JpaRepository<Customer, Long> {
    Customer findByEmail(String email);
}
