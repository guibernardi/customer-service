package exchange.monkey.customer.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class CustomerServiceImpl implements CustomerService  {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Customer save(Customer customer) {
        Customer cus = customerRepository.findByEmail(customer.getEmail());

        if (cus != null) {
            throw new RuntimeException("Customer já existente!");
        }

        cus = customerRepository.save(customer);

        return cus;
    }

    @Override
    public Customer getById(Long id) {
        Customer customer = customerRepository.findById(id).orElse(null);

        if (customer == null) {
            throw new RuntimeException("Customer não encontrado!");
        }

        return customer;
    }
}
