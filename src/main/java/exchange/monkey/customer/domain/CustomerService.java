package exchange.monkey.customer.domain;

public interface CustomerService {
    Customer save(Customer customer);
    Customer getById(Long id);
}
