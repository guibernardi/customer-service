package exchange.monkey.customer.api.v1;

import exchange.monkey.customer.domain.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/v1/customers")
public class CustomerRestService {

    private final CustomerService customerService;
    private final CustomerResourceAssembler customerResourceAssembler;

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public CustomerResource save(@RequestBody CustomerResource customerResource) {
        return customerResourceAssembler.toResource(customerService.save(customerResourceAssembler.toDomain(customerResource)));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET)
    public CustomerResource getById(@PathVariable Long id) {
        return customerResourceAssembler.toResource(customerService.getById(id));
    }
}
