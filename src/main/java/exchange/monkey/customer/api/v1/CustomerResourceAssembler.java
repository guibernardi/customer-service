package exchange.monkey.customer.api.v1;

import exchange.monkey.customer.domain.Customer;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class CustomerResourceAssembler extends ResourceAssemblerSupport<Customer, CustomerResource> {

    public CustomerResourceAssembler() {
        super(CustomerRestService.class, CustomerResource.class);
    }

    @Override
    public CustomerResource toResource(Customer customer) {
        CustomerResource customerResource = createResourceWithId(customer.getId(), customer);
        customerResource.setNome(customer.getNome());
        customerResource.setEmail(customer.getEmail());
        customerResource.setTelefone(customer.getTelefone());
        return customerResource;
    }

    public Customer toDomain(CustomerResource customerResource) {
        return Customer.builder()
                .nome(customerResource.getNome())
                .email(customerResource.getEmail())
                .telefone(customerResource.getTelefone())
                .build();
    }
}

